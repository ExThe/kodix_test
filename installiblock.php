<?require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("iblock");

$arFields = Array(
    'ID' => 'bids',
    'LANG'=>Array(
        'ru'=>Array(
            'NAME'=>'Заявки',
        )
    )
);
$type = new CIBlockType;
$type->Add($arFields);

$arFields = array(
    "ACTIVE" => "Y",
    "NAME" => "Заявки",
    "CODE" => "BIDS",
    "LIST_PAGE_URL" => "",
    "DETAIL_PAGE_URL" => "",
    "IBLOCK_TYPE_ID" => "bids",
    "SITE_ID" => SITE_ID,
    "SORT" => 100,
);
$ibl = new CIBlock;
$iblID = $ibl->Add($arFields);

$props = array(
    array(
        "NAME" => "Имя",
        "CODE" => "NAME",
        "PROPERTY_TYPE" => "S",
        "IBLOCK_ID" => $iblID,
        "IS_REQUIRED" => "Y"
    ),
    array(
        "NAME" => "Телефон",
        "CODE" => "PHONE",
        "PROPERTY_TYPE" => "S",
        "IBLOCK_ID" => $iblID,
        "IS_REQUIRED" => "Y"
    ),
    array(
        "NAME" => "Email",
        "CODE" => "EMAIL",
        "PROPERTY_TYPE" => "S",
        "IBLOCK_ID" => $iblID,
        "IS_REQUIRED" => "Y"
    ),
    array(
        "NAME" => "Индекс",
        "CODE" => "ZIP_CODE",
        "PROPERTY_TYPE" => "N",
        "IBLOCK_ID" => $iblID,
        "IS_REQUIRED" => "N"
    ),
    array(
        "NAME" => "Город",
        "CODE" => "CITY",
        "PROPERTY_TYPE" => "L",
        "IBLOCK_ID" => $iblID,
        "IS_REQUIRED" => "N",
        "LIST_TYPE" => "L",
        "VALUES" => array(
            array(
                "VALUE" => "Москва",
                "DEF" => "N",
                "SORT" => "100"
            ),
            array(
                "VALUE" => "Владимир",
                "DEF" => "N",
                "SORT" => "100"
            ),
            array(
                "VALUE" => "Тула",
                "DEF" => "N",
                "SORT" => "100"
            ),
            array(
                "VALUE" => "Тверь",
                "DEF" => "N",
                "SORT" => "100"
            ),
        )
    ),
    array(
        "NAME" => "Адрес",
        "CODE" => "ADDRESS",
        "PROPERTY_TYPE" => "S",
        "IBLOCK_ID" => $iblID,
        "IS_REQUIRED" => "Y",
        "USER_TYPE" => "HTML"
    ),
    array(
        "NAME" => "Тема обращения",
        "CODE" => "SUBJECT",
        "PROPERTY_TYPE" => "L",
        "IBLOCK_ID" => $iblID,
        "IS_REQUIRED" => "N",
        "LIST_TYPE" => "L",
        "VALUES" => array(
            array(
                "VALUE" => "Вопрос",
                "DEF" => "N",
                "SORT" => "100"
            ),
            array(
                "VALUE" => "Жалоба",
                "DEF" => "N",
                "SORT" => "100"
            ),
            array(
                "VALUE" => "Предложение",
                "DEF" => "N",
                "SORT" => "100"
            ),
            array(
                "VALUE" => "Другая",
                "DEF" => "Y",
                "SORT" => "100"
            ),
        )
    ),
    array(
        "NAME" => "Сообщение",
        "CODE" => "MESSAGE",
        "PROPERTY_TYPE" => "S",
        "IBLOCK_ID" => $iblID,
        "IS_REQUIRED" => "Y",
        "USER_TYPE" => "HTML"
    ),
    array(
        "NAME" => "Согласны ли вы на обработку ваших персональных данных",
        "CODE" => "AGREE",
        "PROPERTY_TYPE" => "L",
        "IBLOCK_ID" => $iblID,
        "IS_REQUIRED" => "Y",
        "LIST_TYPE" => "C",
        "VALUES" => array(
            array(
                "VALUE" => "Да",
                "DEF" => "N",
                "SORT" => "100"
            ),
        )
    ),
);
$p = new CIBlockProperty;
foreach($props as $prop) {
    $p->Add($prop);
}

$et = new CEventType;
$et->Add(array("LID" => SITE_ID, "EVENT_NAME" => "NEW_BID", "NAME" => "Новая заявка"));

$emess = new CEventMessage;
$emess->Add(array(
    "ACTIVE" => "Y",
    "EVENT_NAME" => "NEW_BID",
    "LID" => SITE_ID,
    "EMAIL_FROM" => "#DEFAULT_EMAIL_FROM#",
    "EMAIL_TO" => "#EMAIL#",
    "MESSAGE" => "Ваша заявка принята, ей присвоен номер #ELEM_ID#",
    "SUBJECT" => "#SITE_NAME#: новая заявка!",
));

$emess->Add(array(
    "ACTIVE" => "Y",
    "EVENT_NAME" => "NEW_BID",
    "LID" => SITE_ID,
    "EMAIL_FROM" => "#DEFAULT_EMAIL_FROM#",
    "EMAIL_TO" => "#DEFAULT_EMAIL_FROM#",
    "MESSAGE" => "Поступила новая заявка с сайта.<br>Номер заявки: #ELEM_ID#",
    "SUBJECT" => "#SITE_NAME#: новая заявка!",
    "BODY_TYPE" => "html",
));