<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

foreach($arResult['ERRORS'] as $error) ShowError($error);
?>
<?if($_GET['newbid']):?>
<div class="message">
    Ваша заявка принята, ей присвоен номер <?=$_GET['newbid'];?>
</div>
<? return; endif;?>

<form name="add" method="post" action="" enctype="multipart/form-data">
<?foreach($arResult['FIELDS'] as $field):?>
    <div class="form-item">
        <div class="field-name<?if($field['ERROR'] == 'Y'):?> error<?endif;?>"><?=$field['NAME'];?><?if($field['IS_REQUIRED'] == 'Y'):?>(*)<?endif?></div>
        <div class="field-value">
            <?if($field['PROPERTY_TYPE'] == 'S'):?>
                <?if(empty($field['USER_TYPE'])):?>
                    <input type="text" name="<?=$field['CODE'];?>" value="<?=$field['VALUE'];?>"<?if($field['IS_REQUIRED'] == 'Y'):?> required<?endif;?><?if($field['ERROR'] == 'Y'):?> class="error"<?endif;?>>
                <?else:?>
                    <textarea name="<?=$field['CODE'];?>" <?if($field['IS_REQUIRED'] == 'Y'):?> required<?endif;?><?if($field['ERROR'] == 'Y'):?> class="error"<?endif;?> cols="<?=$field['COL_COUNT'];?>" rows="<?=$field['ROW_COUNT'];?>"><?=$field['VALUE'];?></textarea>
                <?endif;?>
            <?elseif($field['PROPERTY_TYPE'] == 'N'):?>
                <input type="number" name="<?=$field['CODE'];?>" value="<?=$field['VALUE'];?>"<?if($field['IS_REQUIRED'] == 'Y'):?> required<?endif;?><?if($field['ERROR'] == 'Y'):?> class="error"<?endif;?>>
            <?elseif($field['PROPERTY_TYPE'] == 'L'):?>
                <?if($field['LIST_TYPE'] == 'L'):?>
                    <select name="<?=$field['CODE'];?>"<?if($field['IS_REQUIRED'] == 'Y'):?> required<?endif;?><?if($field['ERROR'] == 'Y'):?> class="error"<?endif;?><?if($field['MULTIPLE'] == 'Y'):?> multiple<?endif;?>>
                        <option value="">--</option>
                        <?foreach($field['VALUES'] as $val):?>
                            <option value="<?=$val['ID'];?>"<?if($val['CHECKED'] == 'Y'):?> selected<?endif;?>><?=$val['VALUE'];?></option>
                        <?endforeach;?>
                    </select>
                <?elseif ($field['LIST_TYPE'] == 'C'):?>
                    <?if($field['MULTIPLE'] == 'Y' || count($field['VALUES']) == 1):?>
                        <?foreach($field['VALUES'] as $val):?>
                            <label for="<?=$field['NAME'];?>">
                                <?=$val['VALUE'];?>
                                <input type="checkbox" name="<?=$field['CODE'];?>" value="<?=$val['ID'];?>"<?if($val['CHECKED'] == 'Y'):?> checked<?endif;?>>
                            </label>
                        <?endforeach;?>
                    <?else:?>
                        <?foreach($field['VALUES'] as $val):?>
                            <label for="<?=$field['NAME'];?>">
                                <?=$val['VALUE'];?>
                                <input type="radio" name="<?=$field['CODE'];?>" value="<?=$val['ID'];?>"<?if($val['CHECKED'] == 'Y'):?> checked<?endif;?>>
                            </label>
                        <?endforeach;?>
                    <?endif;?>
                <?endif;?>
            <?elseif($field['PROPERTY_TYPE'] == 'F'):?>
                <?=CFile::InputFile($field['CODE'], 20, 0);?>
            <?endif;?>
        </div>
    </div>
<?endforeach;?>
    <div class="form-item">
        <div class="field-name">Введите код с картинки (*)</div>
        <div class="field-value">
            <input type="hidden" name="captcha_sid" value="<?=$arResult['CAPTCHA_CODE'];?>" />
            <input type="text" name="captcha_word" value=""/>
            <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult['CAPTCHA_CODE'];?>" alt="CAPTCHA" />
        </div>
    </div>
    <input type="submit" name="add_element" value="Отправить">
</form>
<?//echo '<pre>'; print_r($arResult['FIELDS']); echo '</pre>';?>
