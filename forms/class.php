<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Iblock\PropertyTable;
use \Bitrix\Iblock\PropertyEnumerationTable;

\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);

class Forms extends \CBitrixComponent {

    private function addError($text) {
        $this->arResult['ERRORS'][] = $text;
    }

    public function onPrepareComponentParams($arParams)
    {
        if ($arParams["CACHE_TYPE"] == "Y" || ($arParams["CACHE_TYPE"] == "A" && Bitrix\Main\Config\Option::get("main", "component_cache_on", "Y") == "Y"))
            $arParams["CACHE_TIME"] = intval($arParams["CACHE_TIME"]);
        else
            $arParams["CACHE_TIME"] = 0;
        return $arParams;
    }

    private function setValues() {
        foreach($this->arResult['FIELDS'] as &$field) {
            if($field['PROPERTY_TYPE'] == 'L') {
                foreach ($field['VALUES'] as &$val) {
                    if($val['DEF'] == 'Y') {
                        $field['DEFAULT_VALUE'] = $val['ID'];
                        if(!$_POST[$field['CODE']]) {
                            $val['CHECKED'] = 'Y';
                            break;
                        }
                    }
                    if($_POST[$field['CODE']] == $val['ID']) $val['CHECKED'] = 'Y';
                }
            }
            if($field['USER_TYPE'] != '') {
                $field['DEFAULT_VALUE'] = unserialize($field['DEFAULT_VALUE']);
                $field['DEFAULT_VALUE'] = $field['DEFAULT_VALUE']["TEXT"];
            }
            $field['VALUE'] = ($_POST[$field['CODE']]) ? $_POST[$field['CODE']] : $field['DEFAULT_VALUE'];
        }
    }
    protected function setCache() {
        global $USER;
        $this->obCache = new CPHPCache;
        $this->cache_id = SITE_ID."|forms|".serialize($this->arParams)."|".$USER->GetGroups();
        $this->cache_path = "/".SITE_ID . $this->getRelativePath();
        return $this->obCache->StartDataCache($this->arParams["CACHE_TIME"], $this->cache_id, $this->cache_path);
    }

    public function getProperties() {
        if($this->setCache()) {
            $props = PropertyTable::getList(array('order' => array("SORT" => "ASC"), 'filter' => array("IBLOCK_ID" => $this->arParams['IBLOCK_ID'], "!CODE" => $this->arParams['EXCLUDE_PROPERTY'])));
            while($prop = $props->fetch()) {
                if($prop['PROPERTY_TYPE'] == 'L') {
                    $prop['VALUES'] = PropertyEnumerationTable::getList(array('order' => array("SORT" => "ASC"), 'filter' => array("PROPERTY_ID" => $prop['ID'])))->fetchAll();
                }
                $this->arResult['FIELDS'][] = $prop;
            }

            $this->obCache->EndDataCache(
                array(
                    "FIELDS" => $this->arResult['FIELDS'],
                )
            );
        }
        else {
            $vars = $this->obCache->GetVars();
            $this->arResult['FIELDS'] = $vars["FIELDS"];
        }
        $this->setValues();
    }

    private function sendLetter($id, $email) {
        CEvent::Send("NEW_BID", SITE_ID, array("ELEM_ID" => $id, "EMAIL" => $email));
    }

    public function checkFields(&$data) {

        foreach($this->arResult['FIELDS'] as &$field) {
            if($field['IS_REQUIRED'] == 'Y' && empty($_POST[$field['CODE']])) {
                $field['ERROR'] = 'Y';
                $this->addError(GetMessage("REQUIRE_FIELD_EMPTY", array("FIELD_NAME" => $field['NAME'])));
            }
            if($field['CODE'] == 'EMAIL' && !check_email($data[$field['CODE']])) {
                $field['ERROR'] = 'Y';
                $this->addError(GetMessage("INCORRECT_EMAIL"));
            }

            if(empty($data[$field['CODE']]) && !empty($field['DEFAULT_VALUE'])) $data[$field['CODE']] = $field['DEFAULT_VALUE'];
            if($field['USER_TYPE'] != '') $data[$field['CODE']] = array("VALUE" => array("TEXT" => $data[$field['CODE']], "TYPE" => 'TEXT'));


            if($field['PROPERTY_TYPE'] == 'F' && !empty($_FILES[$field['CODE']])) $data[$field['CODE']] = $_FILES[$field['CODE']];


            $props[$field['CODE']] = $data[$field['CODE']];
        }
        $data = $props;
    }

    public function add($data) {

        if (!CMain::CaptchaCheckCode($data["captcha_word"], $data["captcha_sid"])) {
            $this->addError(GetMessage("INCORRECT_CAPTCHA"));
        }
        $this->checkFields($data);

        if(empty($this->arResult['ERRORS'])) {
            CModule::IncludeModule("iblock");
            $arFields = array(
                "IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
                "NAME" => date("d.m.Y H:i:s"),
                "ACTIVE" => "N",
                "PROPERTY_VALUES" => $data
            );
            $el = new CIBlockElement;
            if ($ID = $el->Add($arFields)) {
                $this->sendLetter($ID, $data['EMAIL']);
                LocalRedirect("?newbid=$ID");
            }
            else $this->addError($el->LAST_ERROR);
        }
    }

    public function executeComponent() {
        $this->getProperties();

        if(!strcmp($_SERVER['REQUEST_METHOD'], 'POST') && !empty($_POST)) $this->add($_POST);
        $this->arResult['CAPTCHA_CODE'] = CMain::CaptchaGetCode();
        $this->includeComponentTemplate();
    }
}